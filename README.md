# Welcome to Agersens LoRa Security Module

This is a Golang module with security library extracted from collarfirmware repo.
![agersens-lora-protocol](imgs/agersens-lora-protocol.png)

## Install

```sh
# tbd
```

## Example

You can include this library into your Golang project and use it.
There is an example application which uses this library.

Create a main.go and paste the following code within:  

```go
package main

import (
	"fmt"

	agersenslorasecurity "bitbucket.org/pauldengagersens/lora_lxgroup_security_golang_module"
)

func main() {
	// print version number of the security module
	agersenslorasecurity.Version()

	// initilize variables required
	var input = []byte{0, 1, 2, 3}
	var meta agersenslorasecurity.MessageMetaData
	meta.PacketHeader.NetworkID = 0x1007

	// example of encrypting
	encryptedArray := agersenslorasecurity.Encrypt(input, 1, meta)
	fmt.Printf("%x\r\n", encryptedArray)

	// example of decrypting
	decryptedArray := agersenslorasecurity.Decrypt(input, 1, meta)
	fmt.Printf("%x\r\n", decryptedArray)
}

```

In the project folder:

```sh
go mod init hello

go run main.go
```

## Compile

tbd

## License

Copyright © 2020, [Agersens](https://agersens.com).
