package agersenslorasecurity

import "fmt"

const versionNumber string = "0.0.1"

const encryption int = 0
const decryption int = 1

type MessageMetaData struct {
	PacketHeader struct {
		NetworkID     int `json:"networkId"`
		PacketVersion int `json:"packetVersion"`
		Random        int `json:"random"`
	} `json:"packetHeader"`
	PayloadHeader struct {
		Crc            int    `json:"crc"`
		PacketNumber   int    `json:"packetNumber"`
		MessageType    int    `json:"messageType"`
		Unicast        int    `json:"unicast"`
		RequiresAck    int    `json:"requiresAck"`
		MiscValue      int    `json:"miscValue"`
		SenderUID      string `json:"senderUID"`
		DestinationUID string `json:"destinationUID"`
	} `json:"payloadHeader"`
}

func Version() {
	fmt.Println("Agersens Private LoRa Security Module " + versionNumber)
}

func Encrypt(inputBuffer []byte, encryptionLevel int, messageMeta MessageMetaData) []byte {
	var encrypted = []byte{0, 1, 2, 3}
	return encrypted
}

func Decrypt(inputBuffer []byte, encryptionLevel int, messageMeta MessageMetaData) []byte {
	var decrypted = []byte{3, 2, 1, 0}
	return decrypted
}
