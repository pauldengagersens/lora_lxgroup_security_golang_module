#include <stdint.h>
#include <stdbool.h>

// this was originally defined in Module/LoRa/LoRa.h file
// original value ix 0xF8, as max internal size is 255 bytes. A few extra bytes are lost from alignment.
// since byte array need to change to hex string to pass to Javascript, the buffer size now increased to 1024 bytes
#define PROJ__LORA__MAX_PACKET_SIZE             0x400

// these were originally defined in Module/LoRa/LoRa_Protected.h file
typedef union
{
    struct
    {
        uint16_t        networkId;
        uint8_t         packetVersion;
        uint8_t         random;
    };
    uint32_t            asU32;
} tLoRa_PacketHeader;                              //!< The header of a LoRa packet.

typedef struct
{
    uint16_t        crc;
    uint16_t        packetNumber;
    struct
    {
        uint16_t    messageType : 14;
        uint16_t    unicast     : 1;
        uint16_t    requiresAck : 1;
    };
    uint16_t        miscValue;
    uint64_t        senderUID;
} tLoRa_GenericNetwork;

typedef tLoRa_GenericNetwork tLoRa_BroadcastNetwork;        //!< The broadcast version of a LoRa Network Layer.

typedef struct
{
    uint16_t        crc;
    uint16_t        packetNumber;
    struct
    {
        uint16_t    messageType : 14;
        uint16_t    unicast     : 1;
        uint16_t    requiresAck : 1;
    };
    uint16_t        miscValue;
    uint64_t        senderUID;
    uint64_t        destinationUID;
} tLoRa_UnicastNetwork;                            //!< The unicast version of a LoRa Network Layer.

typedef struct
{
    uint16_t        crc;
    uint8_t         size;
    uint8_t         message[1];                             // Note: the array size is a dummy value.
} tLoRa_Payload;                                   //!< The payload of a LoRa packet.

typedef union
{
    struct
    {
        tLoRa_PacketHeader      header;
        tLoRa_GenericNetwork    network;
    };

    struct
    {
        tLoRa_PacketHeader      header;
        tLoRa_UnicastNetwork    network;
        tLoRa_Payload           payload;
    } unicast;

    struct
    {
        tLoRa_PacketHeader      header;
        tLoRa_BroadcastNetwork  network;
        tLoRa_Payload           payload;
    } broadcast;

    uint8_t asBytes[PROJ__LORA__MAX_PACKET_SIZE];
} tLoRa_Packet;                                             //!< The general form of a LoRa packet.

// this was a private function in Module/LoRa/LoRa.c file
uint64_t LoRa_packetInitVector(bool payload, tLoRa_Packet const* pPacket);