//!
//! @file CRC16.h
//! @brief Modbus-compliant CRC16 calculations. [Header]
//!
//! @author Mac Harwood (Mac.H\@lx-group.com.au  / MacHarwood\@gmail.com)
//!
//! @version 1.0
#ifndef __FILE__CRC_16_DOT_H
#define __FILE__CRC_16_DOT_H
	#include <stdint.h>

	uint16_t crc_Init_(void);
	uint8_t  crc_Do_u8(uint16_t* crcPoint,uint8_t val);
	uint16_t crc_Do_u16_LsbFirst(uint16_t* crcPoint,uint16_t val);
	uint32_t crc_Do_u32(uint16_t* crc, uint32_t val);
	uint16_t crc_Do_Block_u8(const uint8_t* data, int numBytes);

	#if UNIT_TESTS_ENABLED
		void CRC16_UnitTests(void);
	#endif

#endif
