#include "LoRa_Utils.h"
#include <stdio.h>
#include "../config.h"

// this was a private function in Module/LoRa/LoRa.c file
uint64_t LoRa_packetInitVector(bool payload, tLoRa_Packet const* pPacket)
{
    uint64_t returnVector = pPacket->header.asU32;

    if (payload)
    {
        uint64_t highVector = (pPacket->network.packetNumber << 16) | (pPacket->network.messageType << 8) | pPacket->network.miscValue;

        returnVector |= (highVector << 32);
    }

    return returnVector;
}