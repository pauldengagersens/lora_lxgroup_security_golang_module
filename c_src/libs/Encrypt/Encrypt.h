//! @file Encrypt.h
//! @brief This file performs basic encryption and decryption
//!
//! @author Anton Delprado
//! @date 2016/01/18
#ifndef ENCRYPT__H
#define ENCRYPT__H

#include <stdbool.h>
#include <stdint.h>

// ==========================================================
//  ____        _     _ _        _____                       .
// |  _ \ _   _| |__ | (_) ___  |_   _|   _ _ __   ___  ___  .
// | |_) | | | | '_ \| | |/ __|   | || | | | '_ \ / _ \/ __| .
// |  __/| |_| | |_) | | | (__    | || |_| | |_) |  __/\__ \ .
// |_|    \__,_|_.__/|_|_|\___|   |_| \__, | .__/ \___||___/ .
//                                    |___/|_|               .
// ----------------------------------------------------------
typedef struct
{
    uint32_t word0;
    uint32_t word1;
    uint32_t word2;
    uint32_t word3;
} tEncrypt_Key;

#define ENCRYPT__FLAGS__NONE            0x00
#define ENCRYPT__FLAGS__PAD_BUFFER      0x01
#define ENCRYPT__FLAGS__USE_CHECKSUM    0x02
#define ENCRYPT__FLAGS__USE_CBC         0x04

// this was originally defined in Module/LoRa/LoRa.c file
#define     ENCRYPT_FLAGS                   ENCRYPT__FLAGS__USE_CBC

// Added for this lib
#define PAYLOAD_SECURITY 0
#define NETWORK_SECURITY 1
#define ENCRYPTION 0
#define DECRYPTION 1


// =====================================================================
//  ____        _     _ _        __  __      _   _               _      .
// |  _ \ _   _| |__ | (_) ___  |  \/  | ___| |_| |__   ___   __| |___  .
// | |_) | | | | '_ \| | |/ __| | |\/| |/ _ \ __| '_ \ / _ \ / _` / __| .
// |  __/| |_| | |_) | | | (__  | |  | |  __/ |_| | | | (_) | (_| \__ \ .
// |_|    \__,_|_.__/|_|_|\___| |_|  |_|\___|\__|_| |_|\___/ \__,_|___/ .
// ---------------------------------------------------------------------
bool    Encrypt_Encrypt(void* pData, uint16_t size, const tEncrypt_Key* key, uint8_t flags, uint64_t initVector);
bool    Encrypt_Decrypt(void* pData, uint16_t size, const tEncrypt_Key* key, uint8_t flags, uint64_t initVector);
uint8_t Encrypt_PaddingSize(uint16_t size);


#endif
