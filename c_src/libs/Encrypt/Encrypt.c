//! @file Encrypt.c
//! @brief This file performs basic encryption and decryption
//!
//! @author Anton Delprado
//! @date 2016/01/18

//! @page Interface to Encryption Module.
//! This is the interface to the encryption module. Encryption is perfored using the Tiny Encryption Algorithm (TEA).
//! TEA is not infallible, check the Wikipedia page for details: https://en.wikipedia.org/wiki/Tiny_Encryption_Algorithm.\n
//! TEA was chosen largely for its simplicity. Known issues with TEA is that it only encrypts 64 bits at a time.
//! This means that each 64 bits is encrypted separately and can thus be combined in different orders etc by someone trying to break it.
//! A simple solution to this is to use a check sum.\n
//! If robust encryption is required please research it properly. TEA was chosen as a quick "good enough" solution so beware.\n
//! Note that the TEA implementation casts between 8 and 32 bit integers so the encryption and decryption should be performed on similarly endian systems.
//! \n
//! Main public functions in this module are:\n
//! * @ref Encrypt_Encrypt() -- Encrypt some data.\n
//! * @ref Encrypt_Decrypt() -- Decrypt some data.\n
//! * @ref Encrypt_CheckSum() -- Calculate the check sum of a buffer.\n
//! * @ref Encrypt_SetKey() -- Set the encryption/decryption key to a non-default value.

#include <string.h>

#include "Encrypt.h"

//#include "Terminal/Terminal.h"
#include <stdio.h>
#include "../Utils/CRC16.h"
#include "../config.h"

//==================================================================
//   ____             __ _                       _   _              .
//  / ___|___  _ __  / _(_) __ _ _   _ _ __ __ _| |_(_) ___  _ __   .
// | |   / _ \| '_ \| |_| |/ _` | | | | '__/ _` | __| |/ _ \| '_ \  .
// | |__| (_) | | | |  _| | (_| | |_| | | | (_| | |_| | (_) | | | | .
//  \____\___/|_| |_|_| |_|\__, |\__,_|_|  \__,_|\__|_|\___/|_| |_| .
//                         |___/                                    .
//------------------------------------------------------------------

//! Value used to increment a step in TEA encryption/decryption
static const uint32_t       gEncrypt_TEA_Delta	    = 0x9e3779b9;
//! Magic number used for TEA decryption
static const uint32_t       gEncrypt_TEA_SumInverse	= 0xC6EF3720;
//! The size of a block when encrypting using TEA
static const uint8_t        gEncrypt_TEA_BlockSize  = 8;

//===========================================================================
//  ____       _            _         __  __      _   _               _      .
// |  _ \ _ __(_)_   ____ _| |_ ___  |  \/  | ___| |_| |__   ___   __| |___  .
// | |_) | '__| \ \ / / _` | __/ _ \ | |\/| |/ _ \ __| '_ \ / _ \ / _` / __| .
// |  __/| |  | |\ V / (_| | ||  __/ | |  | |  __/ |_| | | | (_) | (_| \__ \ .
// |_|   |_|  |_| \_/ \__,_|\__\___| |_|  |_|\___|\__|_| |_|\___/ \__,_|___/ .
//---------------------------------------------------------------------------

static void Encrypt_TEA_encrypt(uint32_t* pWord0, uint32_t* pWord1, const tEncrypt_Key* pKey);
static void Encrypt_TEA_decrypt(uint32_t* pWord0, uint32_t* pWord1, const tEncrypt_Key* pKey);

//=====================================================================================
//  __  __      _   _               _   ____        __ _       _ _   _                  .
// |  \/  | ___| |_| |__   ___   __| | |  _ \  ___ / _(_)_ __ (_) |_(_) ___  _ __  ___  .
// | |\/| |/ _ \ __| '_ \ / _ \ / _` | | | | |/ _ \ |_| | '_ \| | __| |/ _ \| '_ \/ __| .
// | |  | |  __/ |_| | | | (_) | (_| | | |_| |  __/  _| | | | | | |_| | (_) | | | \__ \ .
// |_|  |_|\___|\__|_| |_|\___/ \__,_| |____/ \___|_| |_|_| |_|_|\__|_|\___/|_| |_|___/ .
//-------------------------------------------------------------------------------------

//==================================================================
//! @brief Encrypt 64 bits of data using the Tiny Encryption Algorithm.
//!
//! @param inWord0		The first 32 bits to encrypt.
//! @param inWord1  	The second 32 bits to encrypt.
//! @param pOutWord0	Where the first 32 bits of encrypted data will be stored.
//! @param pOutWord1	Where the second 32 bits of encrypted data will be stored.
//! @param pKey			The key to use for encryption.
//------------------------------------------------------------------
static void Encrypt_TEA_encrypt(uint32_t* pWord0, uint32_t* pWord1, const tEncrypt_Key* pKey)
{
    if ((pWord0 != NULL) || (pWord1 != NULL) || (pKey != NULL))
    {
        uint32_t word0  = *pWord0;
        uint32_t word1  = *pWord1;
        uint32_t sum    = 0;
        uint32_t index;

        for (index = 0; index < 32; index += 1)
        {
            sum += gEncrypt_TEA_Delta;
            word0 += ((word1 << 4) + pKey->word0) ^ (word1 + sum) ^ ((word1 >> 5) + pKey->word1);
            word1 += ((word0 << 4) + pKey->word2) ^ (word0 + sum) ^ ((word0 >> 5) + pKey->word3);
        }

        *pWord0 = word0;
        *pWord1 = word1;
    }
}

//==================================================================
//! @brief Decrypt 64 bits of data using the Tiny Encryption Algorithm.
//!
//! @param inWord0		The first 32 bits to decrypt.
//! @param inWord1  	The second 32 bits to decrypt.
//! @param pOutWord0	Where the first 32 bits of decrypted data will be stored.
//! @param pOutWord1	Where the second 32 bits of decrypted data will be stored.
//! @param pKey			The key to use for decryption.
//------------------------------------------------------------------
static void Encrypt_TEA_decrypt(uint32_t* pWord0, uint32_t* pWord1, const tEncrypt_Key* pKey)
{
    if ((pWord0 != NULL) || (pWord1 != NULL) || (pKey != NULL))
    {
        uint32_t word0  = *pWord0;
        uint32_t word1  = *pWord1;
        uint32_t sum    = gEncrypt_TEA_SumInverse;
        uint32_t index;

        for (index = 0; index < 32; index += 1)
        {
            word1 -= ((word0 << 4) + pKey->word2) ^ (word0 + sum) ^ ((word0 >> 5) + pKey->word3);
            word0 -= ((word1 << 4) + pKey->word0) ^ (word1 + sum) ^ ((word1 >> 5) + pKey->word1);
            sum -= gEncrypt_TEA_Delta;
        }

        // Keep the order of these two lines as they are (word1 is copied before word0), otherwise
        // the assembler will use STMDB instruction and we get unaligned access fault (as this instruction does not support it)
        *pWord1 = word1;
        *pWord0 = word0;
    }
}
//==================================================================
//! @brief Encrypt a buffer.
//! @details Encrypt using the Tiny Encryption Alogrithm to encode
//! the data. This means that the size has to be a multiple of 8
//! bytes and that each block of 8 bytes is encrypted separately.
//! Use a checksum if integrity needs to be validated. NOTE: If
//! you ask for padding then there must be enough memory available
//! in the given buffer to pad.
//!
//! @param pData    The data to be encrypted. It is encrypted in place.
//! @param size     The size of data to encrypt. NOTE: there must be
//!                 enough memory available for padding if enabled.
//! @param pKey     A pointer to the key used in the encryption.
//! @param flags    Flags used by the encryption algorithm. Specifically
//!                 whether to pad data or use a checksum.
//------------------------------------------------------------------
bool Encrypt_Encrypt(void* pData, uint16_t size, const tEncrypt_Key* pKey, uint8_t flags, uint64_t initVector)
{
    bool returnSuccess = false;
    if ((pData == NULL) || (pKey == NULL))
    {
        // Invalid pointers
        printf("Encryption using invalid pointers\n");
    }
    else if ((size == 0) || (((flags & ENCRYPT__FLAGS__PAD_BUFFER) == 0) && ((size % gEncrypt_TEA_BlockSize) != 0)))
    {
        // Bad size.
        printf("Encryption uses multiples of 64 bits of data\n");
    }
    else
    {
        uint16_t        remaining;
        uint32_t*       pWord       = (uint32_t*)pData;
        uint32_t        highVector  = (uint32_t)(initVector >> 32);
        uint32_t        lowVector   = (uint32_t)initVector;

        // Determine padding if required.
        if (((flags & ENCRYPT__FLAGS__PAD_BUFFER) != 0) && ((size % gEncrypt_TEA_BlockSize) != 0))
        {
            uint8_t padding = Encrypt_PaddingSize(size);

            memset((uint8_t*)pData + size, 0, padding);
            size += padding;
        }

        // Calculate checksum if required
        if ((flags & ENCRYPT__FLAGS__USE_CHECKSUM) != 0)
        {
            uint16_t* pCheckSum = (uint16_t*)pData;
            uint8_t* pBytes = (uint8_t*)pData;
            uint8_t index;

            *pCheckSum = crc_Init_();

            // Iterate through bytes starting after the check sum
            for (index = sizeof(*pCheckSum); index < size; index += 1)
            {
                crc_Do_u8(pCheckSum, pBytes[index]);
            }
        }

#ifdef DEBUG_MSG
        uint8_t*       debugData       = (uint8_t*)pData;
        printf("Add crc/len/pad          : ");
        for (int i = 0; i <size; i++){
            printf("%02x", debugData[i]);
        }
        printf("\n");
#endif // DEBUG_MSG

        // Encrypt data
        // The remaining condition used here is because remaining is unsigned and so if there is a mistake then the loop subtraction may underflow.
        // The loop *should* always terminate with remaining = 0.
        for (remaining = size; remaining >= gEncrypt_TEA_BlockSize; remaining -= gEncrypt_TEA_BlockSize)
        {
            if (flags & ENCRYPT__FLAGS__USE_CBC)
            {
                pWord[0] ^= lowVector;
                pWord[1] ^= highVector;
            }

            Encrypt_TEA_encrypt(&pWord[0], &pWord[1], pKey);

            lowVector = pWord[0];
            highVector = pWord[1];

            // Increment pointer
            pWord += gEncrypt_TEA_BlockSize / sizeof(*pWord);
        }

        returnSuccess = true;
    }

    return returnSuccess;
}

//==================================================================
//! @brief Encrypt a buffer.
//! @details Decrypt using the Tiny Encryption Alogrithm to encode
//! the data. This means that the size has to be a multiple of 8
//! bytes and that each block of 8 bytes is encrypted separately.
//! Use a checksum if integrity needs to be validated.
//!
//! @param pData    The data to be decrypted in place.
//! @param size     The quantity of data to decrypt. Must be a
//!                 multiple of 8 bytes.
//! @param pKey     A pointer to the key used in the encryption.
//! @param flags    Flags used by the encryption algorithm. Specifically
//!                 whether to use a checksum.
//------------------------------------------------------------------
bool Encrypt_Decrypt(void* pData, uint16_t size, const tEncrypt_Key* pKey, uint8_t flags, uint64_t initVector)
{
    bool returnSuccess = false;

    if ((pData == NULL) || (pKey == NULL))
    {
        // Invalid pointers
        printf("Decryption using invalid pointers\n");
    }
    else if ((size == 0) || ((size % gEncrypt_TEA_BlockSize) != 0))
    {
        // Bad size.
        printf("Decryption uses multiples of 64 bits of data\n");
    }
    else
    {
        uint16_t        remaining;
        uint32_t*       pWord       = (uint32_t*)pData;
        uint32_t        highVector  = (uint32_t)(initVector >> 32);
        uint32_t        lowVector   = (uint32_t)initVector;

        // Decrypt block by block
        for (remaining = size; remaining > 0; remaining -= gEncrypt_TEA_BlockSize)
        {
            uint32_t word0 = pWord[0];
            uint32_t word1 = pWord[1];

            Encrypt_TEA_decrypt(&pWord[0], &pWord[1], pKey);

            if ((flags & ENCRYPT__FLAGS__USE_CBC) != 0)
            {
                pWord[0] ^= lowVector;
                pWord[1] ^= highVector;

                lowVector = word0;
                highVector = word1;
            }

            // Increment pointers
            pWord += gEncrypt_TEA_BlockSize / sizeof(*pWord);
        }

        if ((flags & ENCRYPT__FLAGS__USE_CHECKSUM) != 0)
        {
            uint16_t* pCheckSum = (uint16_t*)pData;
            uint8_t* pBytes = (uint8_t*)pData;
            uint16_t checkSum = crc_Init_();
            uint8_t index;

            // Iterate through bytes starting after the check sum
            for (index = sizeof(*pCheckSum); index < size; index += 1)
            {
                crc_Do_u8(&checkSum, pBytes[index]);
            }

            if (*pCheckSum == checkSum)
            {
                // valid check sum
                returnSuccess = true;
            }
            else
            {
                // check sum invalid
                returnSuccess = false;
            }
        }
        else
        {
            // No check sum. Nothing to validate.
            returnSuccess = true;
        }
    }

    return returnSuccess;
}

//==================================================================
//! @brief Gives the amount of padding that will be added when
//! encrypting with the padding flag.
//!
//! @param size			The size of the packet to encrypt.
//!
//! @returns			The quantity of padding that will be used.
//------------------------------------------------------------------
uint8_t Encrypt_PaddingSize(uint16_t size)
{
    // This is essentially (-size) % block size
    // Twos complement logic is used to avoid signed integers;
    // to avoid casting problems and modular arithmetic problems.

    uint16_t sizeTwosComplement = 1 + ~size;
    return sizeTwosComplement % gEncrypt_TEA_BlockSize;
}
